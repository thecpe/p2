>**NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Christian Pelaez-Espinosa

### Project 2 Requirements:

*Four Part:*

1. Create new Git repository that is hosted on the Bitbucket servers
2. Include server-side validation from A4.
3. Displays screenshots
4. Chapter Questions (16,17)

#### README.md file should include the following items:

* Screenshots
* Assesment items in list form


#### Assignment Screenshots/Images:

*Database Image(s)*:

![Valid User Form Entry]
![Screen Shot 2016-04-21 at 5.17.39 PM.png](https://bitbucket.org/repo/aB78ka/images/387945048-Screen%20Shot%202016-04-21%20at%205.17.39%20PM.png)
![Passed Validation]
![Screen Shot 2016-04-21 at 5.17.46 PM.png](https://bitbucket.org/repo/aB78ka/images/3966582035-Screen%20Shot%202016-04-21%20at%205.17.46%20PM.png)
![Valid User Form Entry]
![Screen Shot 2016-04-21 at 5.18.27 PM.png](https://bitbucket.org/repo/aB78ka/images/1120283254-Screen%20Shot%202016-04-21%20at%205.18.27%20PM.png)

![Screen Shot 2016-04-21 at 5.18.50 PM.png](https://bitbucket.org/repo/aB78ka/images/3698064058-Screen%20Shot%202016-04-21%20at%205.18.50%20PM.png)

![Screen Shot 2016-04-21 at 5.18.51 PM.png](https://bitbucket.org/repo/aB78ka/images/2105706103-Screen%20Shot%202016-04-21%20at%205.18.51%20PM.png)

![Screen Shot 2016-04-21 at 5.19.27 PM.png](https://bitbucket.org/repo/aB78ka/images/2022565800-Screen%20Shot%202016-04-21%20at%205.19.27%20PM.png)

![Screen Shot 2016-04-21 at 5.20.17 PM.png](https://bitbucket.org/repo/aB78ka/images/3052785705-Screen%20Shot%202016-04-21%20at%205.20.17%20PM.png)

![Screen Shot 2016-04-21 at 5.20.36 PM.png](https://bitbucket.org/repo/aB78ka/images/2982694624-Screen%20Shot%202016-04-21%20at%205.20.36%20PM.png)